import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
const { expect } = chai;
chai.should();
chai.use(chaiAsPromised);
import * as R from "ramda";

import {
  validateNetwork,
  validateMethod,
  validateKey,
  validateVersion,
  validatePlaintextDID
} from "../../src/validations/did";
import { testError, isErrorWithMessage } from "../testHelper";

const validDIDDetails = {
  network: "6666",
  method: "lifeID",
  key: "1123090098aad908adf098",
  version: "1"
};

describe("validations.ts", () => {
  describe("validateNetwork", () => {
    it("should return true for a valid network", () => {
      const result = validateNetwork(validDIDDetails.network);
      result.right().should.equal(true);
    });
    it("should validate network id is not null", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Network identifier cannot be blank."
      );
      testErrorWithMessage(validateNetwork(null).left());
    });
    it("should validate network id is a valid hex string", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Network must be a valid hex string."
      );
      testErrorWithMessage(validateNetwork("12G4").left());
    });
    it("should validate network id is not blank", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Network identifier cannot be blank."
      );
      testErrorWithMessage(validateNetwork("").left());
    });
    it("should validate network id is not too short", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Network identifier should be 4 characters."
      );
      testErrorWithMessage(validateNetwork("123").left());
    });
    it("should validate network id is not too long", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Network identifier should be 4 characters."
      );
      testErrorWithMessage(validateNetwork("12345").left());
    });
  });
  describe("validateMethod", () => {
    it("should return true for a valid method", () => {
      const result = validateMethod(validDIDDetails.method);
      result.right().should.equal(true);
    });
    it("should only allow alpha characters", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method must contain only alpha characters."
      );
      testErrorWithMessage(validateMethod("123-").left());
    });
    it("should not allow a null method", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method cannot be blank."
      );
      testErrorWithMessage(validateMethod(null).left());
    });
    it("should not allow a blank method", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method cannot be blank."
      );
      testErrorWithMessage(validateMethod("").left());
    });
    it("should not allow a method that is too short", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method should be between 3 and 6 characters."
      );
      testErrorWithMessage(validateMethod("ab").left());
    });
    it("should not allow a method that is too long", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method should be between 3 and 6 characters."
      );
      testErrorWithMessage(validateMethod("abcdefg").left());
    });
  });
  describe("validateVersion", () => {
    it("should return true for a valid version", () => {
      const result = validateVersion(validDIDDetails.version);
      result.right().should.equal(true);
    });
    it("should only allow numbers", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Version should be a number."
      );
      testErrorWithMessage(validateVersion("abcdefg").left());
    });
  });
  describe("validateKey", () => {
    it("should return true for a valid key", () => {
      const result = validateKey(validDIDDetails.key);
      result.right().should.equal(true);
    });
    it("should allow only alpha numeric characters", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Key should be a hex value."
      );
      testErrorWithMessage(validateKey("123asdgf12-1234").left());
    });
  });
  describe("validatePlaintextDID", () => {
    it("should return iteslf for a valid did", () => {
      const did = "did:lifeID:2342311df2df9a989823f2247";
      validatePlaintextDID(did)
        .right()
        .should.equal(did);
    });
    it("should throw an error if did is not correctly formatted", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "DID should be in the format of did:xxxx:1235abc."
      );
      testErrorWithMessage(validatePlaintextDID("didlifeid:12345").left());
    });
    it("should thow an error if first section is not a did", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Beginning of DID should start with 'did'."
      );
      testErrorWithMessage(validatePlaintextDID("bid:lifeid:12345").left());
    });
    it("should throw an error if middle section is too long", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method should be between 3 and 6 characters."
      );
      testErrorWithMessage(
        validatePlaintextDID("did:lifeidtoolong:12345").left()
      );
    });
    it("should throw an error if middle section is too short", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method should be between 3 and 6 characters."
      );
      testErrorWithMessage(validatePlaintextDID("did:sr:1234523452345").left());
    });
    it("should throw an error if middle section is not alpha", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "Method must contain only alpha characters."
      );
      testErrorWithMessage(validatePlaintextDID("did:life1d:12345").left());
    });
    it("should throw an error if last section is too short", () => {
      const testErrorWithMessage = isErrorWithMessage(
        "idString must be longer than 8 characters."
      );
      testErrorWithMessage(validatePlaintextDID("did:lifeid:1234").left());
    });
  });
});
