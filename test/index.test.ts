import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
const { expect } = chai;
chai.should();
chai.use(chaiAsPromised);
import * as Bluebird from "bluebird";

declare var Promise: any;
Promise = Bluebird as any;

import {
  createKeyPair,
  createDID,
  decodeDID,
  validateDID,
  signData,
  verifyData,
  recoverPublicKey
} from "../src/index";

describe("index.ts", () => {
  describe("createKeyPair", () => {
    it("should generate a valid keyPair", () => {
      createKeyPair().should.have.keys(["private", "public", "address"]);
    });
  });
  describe("createDID", () => {
    it("should create a valid DID", () => {
      const validDidData = {
        key: "123dfdf909809823",
        method: "lifeID",
        network: "1234",
        version: "1"
      };
      createDID(validDidData).should.equal(
        "did:lifeID:11234123dfdf909809823447b"
      );
    });
    it("should thow an error when missing key", () => {
      const invalidDIDData = {
        method: "lifeID",
        network: "1234",
        version: "1",
        key: null
      };
      (() => createDID(invalidDIDData)).should.throw(
        "DID detail is not valid."
      );
    });
    it("should thow an error when missing method", () => {
      const invalidDIDData = {
        key: "123",
        network: "1234",
        version: "1",
        method: null
      };
      (() => createDID(invalidDIDData)).should.throw("DID detail is not valid");
    });
    it("should thow an error when missing network", () => {
      const invalidDIDData = {
        key: "123",
        method: "123",
        version: "1",
        network: null
      };
      (() => createDID(invalidDIDData)).should.throw(
        "DID detail is not valid."
      );
    });
    it("should thow an error when missing version", () => {
      const invalidDIDData = {
        key: "123",
        method: "123",
        network: "123"
      };
      (() => createDID(invalidDIDData)).should.throw(
        "DID detail is not valid."
      );
    });
  });

  describe("decodeDID", () => {
    it("should decode a valid DID", () => {
      const decoded = decodeDID("did:lifeID:11234123dfdf909809823fd46");
      expect(decoded).to.have.property("version");
      expect(decoded.version).to.equal("1");
      expect(decoded).to.have.property("network");
      expect(decoded.network).to.equal("1234");
      expect(decoded).to.have.property("method");
      expect(decoded.method).to.equal("lifeID");
      expect(decoded).to.have.property("key");
      expect(decoded.key).to.equal("123dfdf909809823");
    });
  });

  describe("validateDID", () => {
    it("should return true when given a valid did", () => {
      const validDID = "did:lifeID:112341523dfdf90980982303b2";
      validateDID(validDID).should.equal(true);
    });
    it("should return an error when given an invalid did", () => {
      const invalidDID =
        "the day is hot; the Capulets, abroad. And if we meet we shall not escape a brawl";
      (() => validateDID(invalidDID)).should.throw(
        "DID length cannot exceed 74 characters."
      );
    });
  });
  it("should throw an error if null input", () => {
    (() => decodeDID(null)).should.throw("Input must be a valid DID string.");
  });
  it("should throw an error if invalid input", () => {
    (() => decodeDID(1)).should.throw("Input must be a valid DID string.");
  });

  describe("signData", () => {
    it("should sign concated and hashed data", () => {
      signData(
        ["123", "456", "7"],
        "37e31a0ae8217746722a42eee0768e91178bc8c48cf835c51310ced3fa9a9618"
      ).should.equal(
        "13433a02fea6dc2bdd386108c343ed956f3e9b6aef76a7a3c12c6def0d05c1a97df4757d14ed3ab078efddd14889bfcdf4c1a085819c62a992c2599bb12a534300"
      );
    });
  });

  describe("verifyData", () => {
    it("should verify a message and signature", () => {
      verifyData(
        ["123", "456", "7"],
        "13433a02fea6dc2bdd386108c343ed956f3e9b6aef76a7a3c12c6def0d05c1a97df4757d14ed3ab078efddd14889bfcdf4c1a085819c62a992c2599bb12a534300",
        "644a930649f6d0e518a7b860163182fb13f0a5213df2d4d02ef376ec5480ddb6ecf956393c114954462b70174ae7fdb01fa56ec346f647c49168724c68b726ef"
      ).should.equal(true);
    });
  });

  describe("recover public key", () => {
    recoverPublicKey(
      ["123", "456", "7"],
      "13433a02fea6dc2bdd386108c343ed956f3e9b6aef76a7a3c12c6def0d05c1a97df4757d14ed3ab078efddd14889bfcdf4c1a085819c62a992c2599bb12a534300"
    ).should.equal(
      "644a930649f6d0e518a7b860163182fb13f0a5213df2d4d02ef376ec5480ddb6ecf956393c114954462b70174ae7fdb01fa56ec346f647c49168724c68b726ef"
    );
  });
});
