import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
chai.should();
chai.use(chaiAsPromised);

import {
  getVersion,
  getNetwork,
  getChecksum,
  getKey,
  parseDID,
  verifyChecksum
} from "../../src/services/did";

import { testError } from "../testHelper";

describe("lifeid-did.ts", () => {
  describe("parseDID", () => {
    it("should return a parsed did", () => {
      const did = "did:life:11234123sdfsdf9s09809823kln2047";
      const parsedDID = parseDID(did);
      parsedDID.right().should.have.property("method");
      parsedDID.right().method.should.equal("life");
      parsedDID.right().should.have.property("version");
      parsedDID.right().version.should.equal("1");
      parsedDID.right().should.have.property("network");
      parsedDID.right().network.should.equal("1234");
      parsedDID.right().should.have.property("key");
      parsedDID.right().key.should.equal("123sdfsdf9s09809823kln");
    });
  });
  describe("idString", () => {
    let idString = "";
    beforeAll(() => {
      idString = "12234ba3451234523456666";
    });
    describe("getVersion", () => {
      it("should get the version from a did id string", () => {
        getVersion(idString).should.equal("1");
      });
    });
    describe("getNetwork", () => {
      it("should get the network from a did id string", () => {
        getNetwork(idString).should.equal("2234");
      });
    });
    describe("getChecksum", () => {
      it("should get the checksum from a did id string", () => {
        getChecksum(idString).should.equal("6666");
      });
    });
    describe("getKey", () => {
      it("should get the key from a did id string", () => {
        getKey(idString).should.equal("ba345123452345");
      });
    });
  });
  describe("verifyChecksum", () => {
    it("should verify a valid decoded did", () => {
      const decodedDID = "did:lifeID:11234123dfdf909809823447b";
      verifyChecksum(decodedDID).should.equal(
        "did:lifeID:11234123dfdf909809823"
      );
    });
    it("should throw error if checksum incorrect.", async () => {
      const decodedDID = "did:lifeID:11234123dfdf90980982303b1";
      await testError(decodedDID, verifyChecksum, "Checksum is incorrect.");
    });
  });
});
