export const VERSION = 1;
export const HEX_REGEX = /^[0-9A-F]+$/i;
export const ALPHA_REGEX = /^[A-Z]+$/i;
export const ALPHA_NUMERIC_REGEX = /^[A-Z0-9]+$/i;
export const NUM_REGEX = /^[0-9]+$/;
export const BASE58_REGEX = /^[1-9A-HJ-NP-Za-km-z]+$/;
