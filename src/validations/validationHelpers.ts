import {
  HEX_REGEX,
  ALPHA_REGEX,
  NUM_REGEX,
  ALPHA_NUMERIC_REGEX,
  BASE58_REGEX
} from "../constants";

function isHex(input: string): boolean {
  return HEX_REGEX.test(input);
}

function isAlphaNumeric(input: string): boolean {
  return ALPHA_NUMERIC_REGEX.test(input);
}

function isAlpha(input: string): boolean {
  return ALPHA_REGEX.test(input);
}

function isNumeric(input: string): boolean {
  return NUM_REGEX.test(input);
}

function isBase58(input: string): boolean {
  return BASE58_REGEX.test(input);
}

export { isHex, isAlphaNumeric, isAlpha, isNumeric, isBase58 };
