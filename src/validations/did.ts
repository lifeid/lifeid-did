import { isHex, isAlpha, isNumeric } from "./validationHelpers";

import { DIDDetails } from "../models/didDetails";
import { Left, Right, Either } from "monet";
import * as R from "ramda";

function validatePlaintextDID(did: string): Either<Error, string> {
  return _validateDIDisString(did)
    .chain(_validateDIDLength)
    .chain(_validateDidSections);
}

function validateDIDDetails(inputData: DIDDetails): Either<Error, DIDDetails> {
  const result = _validateDIDDetails(inputData)
    .chain(() => validateMethod(inputData.method))
    .chain(() => validateVersion(inputData.version))
    .chain(() => validateKey(inputData.key))
    .chain(() => validateNetwork(inputData.network));
  return result.isRight() ? Right(inputData) : Left(result.left());
}

function validateNetwork(network: string): Either<Error, boolean> {
  if (!network) {
    return Left(new Error("Network identifier cannot be blank."));
  }
  if (network.length !== 4) {
    return Left(new Error("Network identifier should be 4 characters."));
  }
  if (!isHex(network)) {
    return Left(new Error("Network must be a valid hex string."));
  }
  return Right(true);
}

function validateMethod(method: string): Either<Error, boolean> {
  if (!method) {
    return Left(new Error("Method cannot be blank."));
  }
  if (method.length < 3 || method.length > 6) {
    return Left(new Error("Method should be between 3 and 6 characters."));
  }
  if (!isAlpha(method)) {
    return Left(new Error("Method must contain only alpha characters."));
  }
  return Right(true);
}

function validateVersion(version: string): Either<Error, boolean> {
  if (!version) {
    return Left(new Error("Version can't be empty."));
  }
  if (!isNumeric(version)) {
    return Left(new Error("Version should be a number."));
  }
  return Right(true);
}

function validateKey(key: string): Either<Error, boolean> {
  if (!key) {
    return Left(new Error("Key can't be blank."));
  }
  if (!isHex(key)) {
    return Left(new Error("Key should be a hex value."));
  }
  return Right(true);
}

function _isDIDDetails(data: DIDDetails) {
  return data && data.version && data.method && data.network && data.key;
}

function _validateDidSections(did: string): Either<Error, string> {
  const sections = R.split(":", did);

  const result = _validateNumberDIDSections(sections)
    .chain(() => _validateDIDFirstSection(sections[0]))
    .chain(() => validateMethod(sections[1]))
    .chain(() => _validateIdString(sections[2]));

  return result.isRight() ? Right(did) : Left(result.left());
}

function _validateDIDLength(did: string): Either<Error, string> {
  if (did.length >= 74) {
    return Left(new Error("DID length cannot exceed 74 characters."));
  }
  return Right(did);
}

function _validateDIDDetails(input: DIDDetails): Either<Error, DIDDetails> {
  if (!_isDIDDetails(input)) {
    return Left(new Error("DID detail is not valid."));
  }
  return Right(input);
}

function _validateDIDisString(input: string): Either<Error, string> {
  if (!input || typeof input !== "string") {
    return Left(new Error("Input must be a valid DID string."));
  }
  return Right(input);
}

function _validateNumberDIDSections(
  didParts: string[]
): Either<Error, string[]> {
  if (didParts.length !== 3) {
    return Left(new Error("DID should be in the format of did:xxxx:1235abc."));
  }
  return Right(didParts);
}

function _validateDIDFirstSection(part: string): Either<Error, boolean> {
  if (part !== "did") {
    return Left(new Error("Beginning of DID should start with 'did'."));
  }
  return Right(true);
}

function _validateIdString(input: string): Either<Error, boolean> {
  if (!input) {
    return Left(new Error("idString can't be blank."));
  }
  if (input.length < 8) {
    return Left(new Error("idString must be longer than 8 characters."));
  }
  if (!isHex(input)) {
    return Left(new Error("idString should be a valid hex string."));
  }
  return Right(true);
}

export {
  validateNetwork,
  validateVersion,
  validateKey,
  validateMethod,
  validateDIDDetails,
  validatePlaintextDID
};
