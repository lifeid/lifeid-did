export interface DIDDetails {
  version: string;
  method: string;
  network: string;
  key: string;
}
