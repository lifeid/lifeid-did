export interface DDODetails {
  did: string;
  name?: string;
  services?: any;
  publicKeys: string[];
  created?: string;
  updated?: string;
}
