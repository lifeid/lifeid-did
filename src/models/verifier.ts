export interface SigObject {
  r: string;
  s: string;
  recoveryParam?: string;
}
