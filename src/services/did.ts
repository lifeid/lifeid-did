import * as R from "ramda";
const { sha3 } = require("ethereumjs-util");

import { DIDDetails } from "../models/didDetails";
import { Left, Right, Either } from "monet";

function buildDIDString(inputData: DIDDetails): Either<Error, string> {
  try {
    const method = R.prop("method")(inputData);
    const idString = _generateId(inputData);
    return Right(_createDIDFromParts(["did", method, idString]));
  } catch (err) {
    return Left(err);
  }
}

function addChecksum(did: string): string {
  return R.pipe(_calculateChecksum, R.concat(did))(did);
}

function verifyChecksum(did: string): string {
  return R.pipe(
    _splitDIDIntoParts,
    _verifyAndRemoveChecksum,
    _joinDIDFromParts
  )(did);
}

function getVersion(idString: string): string {
  return idString.slice(0, 1);
}

function getNetwork(idString: string): string {
  return idString.slice(1, 5);
}

function getChecksum(idString: string): string {
  return idString.slice(-4);
}

function getType(idString: string): string {
  return idString.slice(5, 6);
}
function getKey(idString: string): string {
  return idString.slice(5, -4);
}

function parseDID(decodedString: string): Either<Error, DIDDetails> {
  try {
    const [method, idString] = _removeLastPart(
      _splitDIDIntoParts(decodedString)
    );
    return Right({
      method,
      network: getNetwork(idString),
      key: getKey(idString),
      version: getVersion(idString)
    });
  } catch (err) {
    return Left(new Error(err));
  }
}

function getIDString(did: string): string {
  return R.pipe(_splitDIDIntoParts, R.last)(did);
}

function _verifyAndRemoveChecksum(parts: string[]): string[] {
  const idString: string = _getIdStringFromParts(parts);
  const idStringWithoutChecksum: string = _removeChecksumFromId(idString);
  const checksumTest = _verifyChecksum(_getChecksumFromId(idString));
  checksumTest(idStringWithoutChecksum);
  const updateLastWithPart = _updateLastPart(idStringWithoutChecksum);
  return updateLastWithPart(parts);
}

function _verifyChecksum(
  checksum: string
): (idStringWithoutChecksum: string) => boolean {
  return (idStringWithoutChecksum: string) => {
    const calculatedChecksum = _calculateChecksum(idStringWithoutChecksum);
    if (calculatedChecksum !== checksum) {
      throw new Error("Checksum is incorrect.");
    }
    return true;
  };
}

function _removeChecksumFromId(idString: string): string {
  return idString.slice(0, -4);
}

function _getChecksumFromId(idString: string): string {
  return idString.slice(-4);
}

function _calculateChecksum(input: string): string {
  const idString = R.last(input.split(":"));
  if (!idString) {
    throw new Error("ID string missing.");
  }
  return sha3(idString)
    .toString("hex")
    .slice(0, 4);
}

function _joinDIDFromParts(parts: string[]): string {
  return R.join(":")(parts);
}

function _updateLastPart(item: string) {
  return R.update(2, item);
}

function _removeLastPart(list: string[]): string[] {
  return R.drop(1, list);
}

function _splitDIDIntoParts(did: string): string[] {
  return R.split(":")(did);
}

function _getIdStringFromParts(parts: string[]): string {
  const idString: string | undefined = R.last(parts);
  if (!idString) {
    throw new Error("No Id string.");
  }
  return idString;
}

function _createDIDFromParts(parts: string[]): string {
  return R.join(":", parts);
}

function _generateId({ network, version, key }: DIDDetails): string {
  return R.pipe(R.join(""), addChecksum)([version, network, key]);
}

export {
  getIDString,
  getVersion,
  getNetwork,
  getChecksum,
  getKey,
  getType,
  addChecksum,
  verifyChecksum,
  parseDID,
  buildDIDString
};
